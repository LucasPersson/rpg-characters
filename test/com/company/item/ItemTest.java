package com.company.item;

import com.company.attributes.PrimaryAttributes;
import com.company.exception.InvalidArmorException;
import com.company.exception.InvalidWeaponException;
import com.company.hero.Warrior;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    Weapon testWeapon;
    Warrior testWarrior;
    Armor testArmor;

    @BeforeEach
    void init() {
        testWarrior = new Warrior("Stefan");

    }

    @Test
    void axeWeapon_WarriorWeaponLevelRequired_InvalidWeaponException() {

        //Arrange
        testWeapon = new Weapon("AXE", Slot.WEAPON, 2, WeaponType.AXE, 25, 1.2);
        String expected = "Could not equip Weapon!";
        //Act
        Exception exception = assertThrows(InvalidWeaponException.class, () -> testWarrior.equipWeapon(testWeapon));
        //Assert
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void plateArmor_WarriorArmorLevelRequired_InvalidArmorException() {

        //Arrange
        testArmor = new Armor("PLATE ARMOR", Slot.BODY, 2, ArmorType.PLATE, new PrimaryAttributes(2,0,0));
        String expected = "Could not equip Armor!";
        //Act
        Exception exception = assertThrows(InvalidArmorException.class, () -> testWarrior.equipArmor(testArmor));
        //Assert
        assertEquals(expected, exception.getMessage());
    }


    @Test
    void bowWeapon_WarriorWeaponWrongType_InvalidWeaponException() {

        //Arrange
        testWeapon = new Weapon("Big bow", Slot.WEAPON, 1, WeaponType.BOW, 30, 1.2);
        String expected = "Could not equip Weapon!";
        //Act
        Exception exception = assertThrows(InvalidWeaponException.class, () -> testWarrior.equipWeapon(testWeapon));
        //Assert
        assertEquals(expected, exception.getMessage());
    }


    @Test
    void clothArmor_WarriorArmorWrongType_InvalidArmorException() {

        //Arrange
        testArmor = new Armor("Shirt", Slot.BODY, 1, ArmorType.CLOTH, new PrimaryAttributes(0,0,4));
        String expected = "Could not equip Armor!";
        //Act
        Exception exception = assertThrows(InvalidArmorException.class, () -> testWarrior.equipArmor(testArmor));
        //Assert
        assertEquals(expected, exception.getMessage());
    }


    @Test
    void AxeWeapon_EquipAxeToWarrior_ShouldPass() throws InvalidWeaponException {

        //Arrange
        testWeapon = new Weapon("Axe", Slot.WEAPON, 1, WeaponType.AXE, 20, 0.7);
        boolean actual = true;
        //Act
        boolean expected = testWarrior.equipWeapon(testWeapon);
        //Assert
        assertEquals(expected, actual);
    }


    @Test
    void plateArmor_EquipPlateArmorToWarrior_ShouldPass() throws InvalidArmorException {

        //Arrange
        testArmor = new Armor("Plate chest", Slot.BODY, 1, ArmorType.PLATE, new PrimaryAttributes(4,0,0));
        boolean actual = true;
        //Act
        boolean expected = testWarrior.equipArmor(testArmor);
        //Assert
        assertEquals(expected, actual);
    }


    @Test
    void equipPlateArmor_CheckWarriorTotalAttributes_ShouldPass() throws InvalidArmorException {

        //Arrange
        testArmor = new Armor("Plate chest", Slot.BODY, 1, ArmorType.PLATE, new PrimaryAttributes(4,0,0));
        // warrior level 1 attributes {5,2,1} expected is with armor equipped
        int[] expected = new int[]{9, 2, 1};
        //Act
        testWarrior.equipArmor(testArmor);
        int[] actual = testWarrior.getTotalAttributes();
        //Assert
        assertArrayEquals(expected, actual);
    }


    @Test
    void warriorDps_checkWarriorDps_ShouldPass() {

        //Arrange
        double actual = 1 * (1 + ( 5 / 100.0 ));
        //Act
        double expected = testWarrior.getHeroDPS();
        //Assert
        assertEquals(expected, actual);
    }


    @Test
    void warriorDps_checkWarriorDpsWithAxe_ShouldPass() throws InvalidWeaponException {

        //Arrange
        testWeapon = new Weapon("Axe", Slot.WEAPON, 1, WeaponType.AXE, 20, 0.7);
        double actual = (20 * 0.7) * (1 + ( 5 / 100.0 ));
        //Act
        testWarrior.equipWeapon(testWeapon);
        double expected = testWarrior.getHeroDPS();
        //Assert
        assertEquals(expected, actual);
    }


    @Test
    void warriorDps_checkWarriorDpsWithAxeAndArmor_ShouldPass() throws InvalidWeaponException, InvalidArmorException {

        //Arrange
        testWeapon = new Weapon("Axe", Slot.WEAPON, 1, WeaponType.AXE, 20, 0.7);
        testArmor = new Armor("Plate chest", Slot.BODY, 1, ArmorType.PLATE, new PrimaryAttributes(4,0,0));
        double actual = (20 * 0.7) * (1 + ( 9 / 100.0 ));
        //Act
        testWarrior.equipWeapon(testWeapon);
        testWarrior.equipArmor(testArmor);
        double expected = testWarrior.getHeroDPS();
        //Assert
        assertEquals(expected, actual);
    }
}