package com.company.hero;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {
    Mage testMage;
    Warrior testWarrior;
    Hunter testHunter;
    Rogue testRogue;

    @BeforeEach
    void init() {
        testMage = new Mage("Sarcell");
        testWarrior = new Warrior("Stefan");
        testHunter = new Hunter("Sally");
        testRogue = new Rogue("Sarco");
    }


    @Test
    void getName_CheckMageName_ShouldPass() {

        //Arrange
        String expected = "Sarcell";
        //Act
        String actual = testMage.getName();
        //Assert
        assertEquals(expected, actual);
    }


    @Test
    void getLevel_GetMageLevel_ShouldPass() {

        //Arrange
        int expected = 1;
        //Act
        int actual = testMage.getLevel();
        //Assert
        assertEquals(expected, actual);
    }


    @Test
    void levelUp_LevelUpMage_ShouldPass() {

        //Arrange
        int expected = 2;
        //Act
         testMage.levelUp();
         int actual = testMage.getLevel();
        //Assert
        assertEquals(expected, actual);
    }


    @Test
    void baseAttributes_MageBasePrimaryAttributes_ShouldPass() {

        //Arrange
        int[] expected = {1,1,8};
        //Act
         int[] actual = testMage.getBasePrimaryAttributes();
        //Assert
        assertArrayEquals(expected, actual);
    }

    @Test
    void levelUp_MageLevelUpBasePrimaryAttributes_ShouldPass() {

        //Arrange
        int[] expected = {2,2,13};
        //Act
        testMage.levelUp();
        int[] actual = testMage.getBasePrimaryAttributes();
        //Assert
        assertArrayEquals(expected, actual);
    }


    @Test
    void baseAttributes_WarriorBasePrimaryAttributes_ShouldPass() {

        //Arrange
        int[] expected = {5,2,1};
        //Act
         int[] actual = testWarrior.getBasePrimaryAttributes();
        //Assert
        assertArrayEquals(expected, actual);
    }


    @Test
    void levelUp_WarriorLevelUpBasePrimaryAttributes_ShouldPass() {

        //Arrange
        int[] expected = {8,4,2};
        //Act
        testWarrior.levelUp();
        int[] actual = testWarrior.getBasePrimaryAttributes();
        //Assert
        assertArrayEquals(expected, actual);
    }


    @Test
    void baseAttributes_HunterBasePrimaryAttributes_ShouldPass() {

        //Arrange
        int[] expected = {1,7,1};
        //Act
         int[] actual = testHunter.getBasePrimaryAttributes();
        //Assert
        assertArrayEquals(expected, actual);
    }

    @Test
    void levelUp_HunterLevelUpBasePrimaryAttributes_ShouldPass() {

        //Arrange
        int[] expected = {2,12,2};
        //Act
        testHunter.levelUp();
        int[] actual = testHunter.getBasePrimaryAttributes();
        //Assert
        assertArrayEquals(expected, actual);
    }


    @Test
    void baseAttributes_RogueBasePrimaryAttributes_ShouldPass() {

        //Arrange
        int[] expected = {2,6,1};
        //Act
         int[] actual = testRogue.getBasePrimaryAttributes();
        //Assert
        assertArrayEquals(expected, actual);
    }

    @Test
    void levelUp_RogueLevelUpBasePrimaryAttributes_ShouldPass() {

        //Arrange
        int[] expected = {3,10,2};
        //Act
        testRogue.levelUp();
         int[] actual = testRogue.getBasePrimaryAttributes();
        //Assert
        assertArrayEquals(expected, actual);
    }
}