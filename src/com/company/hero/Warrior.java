package com.company.hero;

import com.company.attributes.PrimaryAttributes;
import com.company.item.ArmorType;
import com.company.item.WeaponType;

public class Warrior extends Hero {
    public Warrior(String name) {
        super(name, new PrimaryAttributes(5, 2, 1),
        new WeaponType[]
        {WeaponType.AXE,
        WeaponType.HAMMER,
        WeaponType.SWORD},
        new ArmorType[]
        {ArmorType.PLATE,
        ArmorType.MAIL});
    }


    @Override
    protected void levelUpAttributesIncrease() {
        basePrimaryAttributes.increaseStrength(3);
        basePrimaryAttributes.increaseDexterity(2);
        basePrimaryAttributes.increaseIntelligence(1);
    }

    @Override
    public int getMainTotalPrimaryAttribute() {
        // array is str, dex, int
            return getTotalAttributes()[0];

    }
}
