package com.company.hero;

import com.company.attributes.PrimaryAttributes;
import com.company.item.ArmorType;
import com.company.item.WeaponType;

public class Rogue extends Hero {
    public Rogue(String name) {
        super(name, new PrimaryAttributes(2, 6, 1),
        new WeaponType[]
        {WeaponType.DAGGER,
        WeaponType.SWORD},
        new ArmorType[]
        {ArmorType.LEATHER,
        ArmorType.MAIL});
    }




    @Override
    protected void levelUpAttributesIncrease() {
        basePrimaryAttributes.increaseStrength(1);
        basePrimaryAttributes.increaseDexterity(4);
        basePrimaryAttributes.increaseIntelligence(1);
    }

    @Override
    public int getMainTotalPrimaryAttribute() {
        // array is str, dex, int
        return getTotalAttributes()[1];

    }
}
