package com.company.hero;

import com.company.attributes.PrimaryAttributes;
import com.company.exception.InvalidArmorException;
import com.company.exception.InvalidWeaponException;
import com.company.item.*;

import java.util.HashMap;

public abstract class Hero {

   private String name;
   private int level = 1;
   protected PrimaryAttributes basePrimaryAttributes;
   private HashMap<Slot, Item> heroEquipment = new HashMap<>();
   private WeaponType[] heroWeaponType;
   private ArmorType[] heroArmorType;


    public Hero(String name, PrimaryAttributes basePrimaryAttributes, WeaponType[] heroWeaponType, ArmorType[] heroArmorType) {
        this.name = name;
        this.basePrimaryAttributes = basePrimaryAttributes;
        this.heroWeaponType = heroWeaponType;
        this.heroArmorType = heroArmorType;

    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    //returns hero attributes without added bonus from equipment
    public int[] getBasePrimaryAttributes() {
        return basePrimaryAttributes.getAttributes();
    }

    public void levelUp() {
        level++;
        levelUpAttributesIncrease();
    }

    //used by the hero subclasses to increase the baseAttributes on levelup
    protected abstract void levelUpAttributesIncrease();

    //gets the total attributes heroes attribute + the equipment attributes
    public int[] getTotalAttributes() {

        int strength = basePrimaryAttributes.getStrength();
        int dexterity = basePrimaryAttributes.getDexterity();
        int intelligence = basePrimaryAttributes.getIntelligence();

        for ( Item item : heroEquipment.values() ) {
            //checks the respective stat on armor piece
            if (item instanceof Armor) {
                strength += ((Armor)item).getArmorAttributes().getStrength();
                dexterity += ((Armor)item).getArmorAttributes().getDexterity();
                intelligence += ((Armor)item).getArmorAttributes().getIntelligence();
            }
        }

        return new int[] {strength, dexterity, intelligence};
    }

    //gets the main attribute from the hero
    public abstract int getMainTotalPrimaryAttribute();


    public double getHeroDPS() {
        if (!heroEquipment.containsKey(Slot.WEAPON)) {
            return 1 * (1 + (getMainTotalPrimaryAttribute() / 100.0));
        } else {
            double weaponDps = ((Weapon)heroEquipment.get(Slot.WEAPON)).getDamage() * ((Weapon)heroEquipment.get(Slot.WEAPON)).getAttackSpeed();
            return weaponDps * (1 + (getMainTotalPrimaryAttribute() / 100.0));
        }
    }

    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {

        if (level < weapon.getRequiredLevel() || !checkIfHeroCanUseWeaponType(weapon.getType())) {
            throw new InvalidWeaponException("Could not equip Weapon!");
        }
        heroEquipment.put(weapon.getSlot(), weapon);
        return true;
    }

    public boolean equipArmor(Armor armor) throws InvalidArmorException {

        if (level < armor.getRequiredLevel() || !checkIfHeroCanUseArmorType(armor.getType())) {
            throw new InvalidArmorException("Could not equip Armor!");
        }

        heroEquipment.put(armor.getSlot(), armor);
        return true;
    }


    public boolean checkIfHeroCanUseWeaponType(WeaponType type) {

        for ( WeaponType weapon : heroWeaponType ) {
            if (weapon == type){
                return true;
            }
        }
        return false;
    }

    public boolean checkIfHeroCanUseArmorType(ArmorType type) {

        for ( ArmorType armor : heroArmorType ) {
            if (armor == type){
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {

        StringBuilder stats = new StringBuilder();
        stats.append("Name: ").append(name).append("\n");
        stats.append("Level: ").append(level).append("\n");
        stats.append("Strength: ").append(getTotalAttributes()[0]).append("\n");
        stats.append("Dexterity: ").append(getTotalAttributes()[1]).append("\n");
        stats.append("Intelligence: ").append(getTotalAttributes()[2]).append("\n");
        stats.append("DPS: ").append(getHeroDPS()).append("\n");

        return stats.toString();

    }
}
