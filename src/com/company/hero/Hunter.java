package com.company.hero;

import com.company.attributes.PrimaryAttributes;
import com.company.item.ArmorType;
import com.company.item.WeaponType;

public class Hunter extends Hero {
    public Hunter(String name) {
        super(name, new PrimaryAttributes(1, 7, 1),
        new WeaponType[]
        {WeaponType.BOW},
        new ArmorType[]
        {ArmorType.LEATHER,
        ArmorType.MAIL});
    }


    @Override
    protected void levelUpAttributesIncrease() {
        basePrimaryAttributes.increaseStrength(1);
        basePrimaryAttributes.increaseDexterity(5);
        basePrimaryAttributes.increaseIntelligence(1);
    }

    @Override
    public int getMainTotalPrimaryAttribute() {
        // array is str, dex, int
        return getTotalAttributes()[1];

    }

}
