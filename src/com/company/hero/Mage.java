package com.company.hero;

import com.company.attributes.PrimaryAttributes;
import com.company.item.ArmorType;
import com.company.item.WeaponType;

public class Mage extends Hero {
    public Mage(String name) {
        super(name, new PrimaryAttributes(1,1,8),
        new WeaponType[]
        {WeaponType.STAFF,
        WeaponType.WAND},
        new ArmorType[]
        {ArmorType.CLOTH});
    }


    @Override
    protected void levelUpAttributesIncrease() {
        basePrimaryAttributes.increaseStrength(1);
        basePrimaryAttributes.increaseDexterity(1);
        basePrimaryAttributes.increaseIntelligence(5);
    }

    @Override
    public int getMainTotalPrimaryAttribute() {
        // array is str, dex, int
        return getTotalAttributes()[2];

    }
}
