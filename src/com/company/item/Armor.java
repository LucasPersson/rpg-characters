package com.company.item;

import com.company.attributes.PrimaryAttributes;

public class Armor extends Item{

    private ArmorType type;
    private PrimaryAttributes armorAttributes;

    public Armor(String name, Slot slot, int requiredLevel, ArmorType type, PrimaryAttributes armorAttributes) {
        super(name, slot, requiredLevel);
        this.type = type;
        this.armorAttributes = armorAttributes;
    }

    public PrimaryAttributes getArmorAttributes() {
        return armorAttributes;
    }

    public ArmorType getType() {
        return type;
    }

    public void setType(ArmorType type) {
        this.type = type;
    }
}
