package com.company.item;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
