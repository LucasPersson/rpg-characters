package com.company.item;

public class Weapon extends Item {

    private WeaponType type;
    private double damage;
    private double attackSpeed;

    public Weapon(String name, Slot slot, int requiredLevel, WeaponType type, double damage, double attackSpeed) {
        super(name, slot, requiredLevel);
        this.type = type;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }

    public double getDamage() {
        return damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public WeaponType getType() {
        return type;
    }

    public void setType(WeaponType type) {
        this.type = type;
    }
}
