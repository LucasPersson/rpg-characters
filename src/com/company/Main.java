package com.company;

import com.company.attributes.PrimaryAttributes;
import com.company.exception.InvalidArmorException;
import com.company.exception.InvalidWeaponException;
import com.company.hero.*;
import com.company.item.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InvalidWeaponException, InvalidArmorException {

        //initiate scanner
        Scanner userInput = new Scanner(System.in);

        System.out.println("Choose your hero class!");
        System.out.println("Mage starts as level 2 with armor and weapon.");
        System.out.println("----------------------------------------");
        System.out.println("1 Hunter: Dexterity based character.");
        System.out.println("2 Mage: Intellect based character.");
        System.out.println("3 Rogue: Dexterity based character.");
        System.out.println("4 Warrior: Strength based character.");
        System.out.println("----------------------------------------");
        System.out.print(" ");

        // number to be used in switch case for the class selected
        int heroClass = userInput.nextInt();
        userInput.nextLine();
        System.out.println("Enter a name for your hero!");
        // name to be used when constructing new hero
        String heroName = userInput.nextLine();


        switch (heroClass) {
            case 1 -> {
                Hunter hunter = new Hunter(heroName);
                System.out.println("Your journey may begin: " + hunter.getName() + " the Guerrilla Marksman.");
                System.out.println(hunter);
            }
            //the mage hero displays a more geared and leveled up character
            case 2 -> {
                Mage mage = new Mage(heroName);
                System.out.println("Your journey may begin: " + mage.getName() + " the Delirious Sage.");
                Weapon weapon = new Weapon("Drift Wood Wand", Slot.WEAPON, 1, WeaponType.WAND, 30, 2.0);
                Armor armor = new Armor("Rags", Slot.BODY, 2, ArmorType.CLOTH, new PrimaryAttributes(2,1,20));
                mage.levelUp();
                mage.equipWeapon(weapon);
                mage.equipArmor(armor);
                System.out.println(mage);
            }
            case 3 -> {
                Rogue rogue = new Rogue(heroName);
                System.out.println("Your journey may begin: " + rogue.getName() + " the Mischievous Shadow.");
                System.out.println(rogue);
            }
            case 4 -> {
                Warrior warrior = new Warrior(heroName);
                System.out.println("Your journey may begin: " + warrior.getName() + " the Warrior Cheif.");
                System.out.println(warrior);
            }
            default -> System.out.println("Invalid!");
        }

    }
}
