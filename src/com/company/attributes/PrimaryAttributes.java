package com.company.attributes;

public class PrimaryAttributes {

    private int strength;
    private int dexterity;
    private int intelligence;

    public PrimaryAttributes(int strength , int dexterity, int intelligence ) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public int[] getAttributes() {
        return new int[]{strength, dexterity, intelligence};
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void increaseStrength(int str) { strength += str ;}

    public void increaseDexterity(int dex) { dexterity += dex ;}

    public void increaseIntelligence(int intellect) { intelligence += intellect ;}


}
