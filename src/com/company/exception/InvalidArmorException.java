package com.company.exception;

public class InvalidArmorException extends Exception {

    public InvalidArmorException(String message) {
        super(message);
    }
}
