# RPG-Character

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

This project was built in Java with [IntelliJ](https://www.jetbrains.com/).

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Assumptions](#Test Assumptions)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

This project was created for an assignment to learn more about OOP and Java.

## Install

This project uses Java. Go checkout the IDE "IntelliJ" that can run the files.

## Usage

Start your IDE and run the Main file. "shift f10" in IntelliJ. Then you can select your hero and name in console.

## Test Assumptions

Not full test coverage of all methods, some getters and setters are assumed to work. As are the other classes since Warrior is tested.
Main file is not tested as I assume that it is not relevant for the assignment.

## Maintainers

[Lucas Persson (@LucasPersson)](https://gitlab.com/LucasPersson)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Lucas Persson
